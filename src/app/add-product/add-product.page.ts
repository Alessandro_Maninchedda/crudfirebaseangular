import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {

  title: string;
  description: string;
  price: number;
  quantity: number;
  InInventorySince: number;

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private loaddingCtrl: LoadingController,
    private toastr: ToastController
  ) { }

  ngOnInit() {
  }

  async addProduct(){
    if(this.title && this.description && this.price && this.quantity && this.InInventorySince){
      const loading = await this.loaddingCtrl.create({
        message: 'add product..',
        spinner: 'crescent',
        showBackdrop: true
    });

    loading.present();

    const productId = this.afs.createId();

    this.afs.collection('product').doc(productId).set({
      'productId': productId,
      'title': this.title,
      'description': this.description,
      'price': this.price,
      'quantity': this.quantity,
      'InInventorySince': this.InInventorySince,
      'status': '',
      'createdAt': Date.now(),
    })
    .then(()=>{
      loading.dismiss();
      this.toast('Task Successfully Added', 'success');
      this.router.navigate(['/products']);
    })
    .catch((error)=>{
      loading.dismiss();
      this.toast(error.message, 'danger');
    })
    }
  } // end of add product
  async toast(msg, status){
    const toast = await this.toastr.create({
      message: msg,
      position: 'top',
      color: status,
      duration: 1500
    });

  toast.present() 
  } // end of toast
}
