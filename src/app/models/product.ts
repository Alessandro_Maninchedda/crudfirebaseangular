export interface Product {
   productId: string;
   title: string;
   description: string;
   price: number;
   quantity: number;
   InInventorySince: number;
   status: string;
   createdAt: number;
}
