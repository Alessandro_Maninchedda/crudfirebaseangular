import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Product } from '../models/product';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
  providers: [ServicesService]
})
export class ProductsPage implements OnInit {

  products: Product[];

  constructor(
    private productService: ServicesService,
    private loadingCtrl: LoadingController,
    private Router: Router,
    private toastr: ToastController
  ) { 

  }

  ngOnInit() {
    this.productService.getProducts().subscribe(products =>{
      this.products = this.products;
    })
  }

}
